﻿

class AverageRecommender(object):
    global_average = 0
    user_average = {}
    item_average = {}
    user_bias = {}
    item_bias = {}

    def learn(data_hash):
        ic = {}
        uc = {}
        ga = 0
        gc = 0
        for u in data_hash:
            ua = 0
            uc = 0
            for i in data_hash[u]:
                AverageRecommender.item_average[i] = AverageRecommender.item_average.get(i,0) + data_hash[u][i]
                ic[i] = ic.get(i,0) + 1
                uc = uc + 1
                ua = ua + data_hash[u][i]
                ga = ga + data_hash[u][i]
                gc = gc + 1
            AverageRecommender.user_average[u] = ua/uc
        AverageRecommender.global_average = ga/gc
        for key in AverageRecommender.item_average:
            AverageRecommender.item_average[key] = AverageRecommender.item_average[key]/ic[key]
            AverageRecommender.item_bias[key] = AverageRecommender.item_average[key] - AverageRecommender.global_average
        for key in AverageRecommender.user_average:
            AverageRecommender.user_bias[key] = AverageRecommender.user_average[key] - AverageRecommender.global_average

    def learn_with_prior(data_hash, k=20):
        ga = 0
        gc = 0
        for u in data_hash:
            for i in data_hash[u]:
                ga = ga + data_hash[u][i]
                gc = gc + 1
        AverageRecommender.global_average = ga/gc
        ic = {}
        uc = {}
        for u in data_hash:
            ua = k*AverageRecommender.global_average
            uc = k
            for i in data_hash[u]:
                AverageRecommender.item_average[i] = AverageRecommender.item_average.get(i,k*AverageRecommender.global_average) + data_hash[u][i]
                ic[i] = ic.get(i,k) + 1
                uc = uc + 1
                ua = ua + data_hash[u][i]
            AverageRecommender.user_average[u] = ua/uc
        for key in AverageRecommender.item_average:
            AverageRecommender.item_average[key] = AverageRecommender.item_average[key]/ic[key]
            AverageRecommender.item_bias[key] = AverageRecommender.item_average[key] - AverageRecommender.global_average
        for key in AverageRecommender.user_average:
            AverageRecommender.user_bias[key] = AverageRecommender.user_average[key] - AverageRecommender.global_average

    def learn_with_gradient_descent(data_hash, lRate=0.01):
        AverageRecommender.learn(data_hash)
        iterations = 50
        for iteration in range(0,iterations):
                for u in data_hash:
                    for i in data_hash[u]:
                       error = (data_hash[u][i] - AverageRecommender.predict(data_hash, u, i))
                       AverageRecommender.global_average = AverageRecommender.global_average + lRate*error
                       AverageRecommender.user_bias[u] = AverageRecommender.user_bias[u] + lRate*error
                       AverageRecommender.item_bias[i] = AverageRecommender.item_bias[i] + lRate*error
    
    def predict(data_hash, user, item):
        return AverageRecommender.global_average + AverageRecommender.user_bias.get(user,0) + AverageRecommender.item_bias.get(item,0)

    def predict_global_average(data_hash, user, item):
        return AverageRecommender.global_average

    def predict_user_average(data_hash, user, item):
        return AverageRecommender.user_average.get(user, AverageRecommender.global_average)
    
    def predict_item_average(data_hash, user, item):
        return AverageRecommender.item_average.get(item, AverageRecommender.global_average)