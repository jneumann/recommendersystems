﻿from AverageRecommender import AverageRecommender
import math
import heapq
import itertools

class KNNRecommender(object):
    """description of class"""
    ch = {}

    def cosine(data_hash, user1, user2, impute_user1, impute_user2):
        productsum = 0
        user1_sum = 0
        user2_sum = 0
        items_user1_has_not_seen = len(AverageRecommender.item_average)-len(data_hash[user1])
        items_user2_has_not_seen = len(AverageRecommender.item_average)-len(data_hash[user2])
        for i in data_hash[user1]:
            productsum = productsum + data_hash[user1].get(i,impute_user1)*data_hash[user2].get(i,impute_user2)
            user1_sum = user1_sum + math.pow(data_hash[user1].get(i,impute_user1),2)
        for i in data_hash[user2]:
            if data_hash[user1].get(i,-1)<0: 
                productsum = productsum + data_hash[user1].get(i,impute_user1)*data_hash[user2].get(i,impute_user2) 
            user2_sum = user2_sum + math.pow(data_hash[user2].get(i,impute_user2),2) 
        user1_sum = user1_sum + items_user1_has_not_seen*math.pow(impute_user1,2)
        user2_sum = user2_sum + items_user2_has_not_seen*math.pow(impute_user2,2)
        return productsum/(math.sqrt(user1_sum)*math.sqrt(user2_sum))

    def cosine_hash(data_hash):
        c = 0
        cos = 0
        for u in data_hash:
            KNNRecommender.ch[u] = {}
        for u, v in itertools.combinations(data_hash,2):
            cos = KNNRecommender.cosine(data_hash, u, v, AverageRecommender.user_average[u], AverageRecommender.user_average[v])
            KNNRecommender.ch[u][v] = cos
            KNNRecommender.ch[v][u] = cos
            c= c +1
            print(c)
                    
    def predict(data_hash, user, item, k=10):
        result = AverageRecommender.user_average.get(user,AverageRecommender.global_average)
        if KNNRecommender.ch.get(user,None) is None: return result
        neighbours = heapq.nlargest(k, KNNRecommender.ch[user], key=KNNRecommender.ch[user].get)
        sum1 = 0
        sum2 = 0
        for n in neighbours:
            sum1 = sum1 + KNNRecommender.ch[user][n]*(data_hash[n].get(item, AverageRecommender.user_average[n]) - AverageRecommender.user_average[n])
            sum2 = sum2 + KNNRecommender.ch[user][n]
        return result + sum1/sum2