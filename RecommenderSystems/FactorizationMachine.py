﻿import math
from AverageRecommender import AverageRecommender


class FactorizationMachine(object):
    """description of class"""


    userfeature = {}
    itemfeature = {}
    otherMoviesWatchedValues = {}
    otherMoviesWatchedFeatures = {}
    otherMoviesWatchedLatentFeatures = {}

    def learn(data_hash , k, lRate=0.01, regRate = 0.01):
        for u in data_hash:
            FactorizationMachine.userfeature[u] = []
            FactorizationMachine.otherMoviesWatchedValues[u] = {}
            for i in data_hash[u]:
                FactorizationMachine.itemfeature[i] = []
                FactorizationMachine.otherMoviesWatchedValues[u][i] = 1/len(data_hash[u])
                FactorizationMachine.otherMoviesWatchedFeatures[i] = 0.1
                FactorizationMachine.otherMoviesWatchedLatentFeatures[i] = []
                for f in range(0,k):                        
                    FactorizationMachine.itemfeature[i].append(0.1)
                    FactorizationMachine.otherMoviesWatchedLatentFeatures[i].append(0.1)
            for f in range(0,k):
                FactorizationMachine.userfeature[u].append(0.1)
        iterations = 10
        for iteration in range(0,iterations):
                for u in data_hash:
                    for i in data_hash[u]:
                            error = (data_hash[u][i] - FactorizationMachine.predict(data_hash, u, i))
                            for f in range(0,len(FactorizationMachine.userfeature[u])):
                               sum = FactorizationMachine.userfeature[u][f] + FactorizationMachine.itemfeature[i][f]
                               for other in data_hash[u]:
                                   if f==0:
                                       FactorizationMachine.otherMoviesWatchedFeatures[other] += lRate*error*FactorizationMachine.otherMoviesWatchedValues[u][other] 
                                   sum+=FactorizationMachine.otherMoviesWatchedLatentFeatures[other][f]*FactorizationMachine.otherMoviesWatchedValues[u][other]
                               FactorizationMachine.userfeature[u][f] += lRate*error*(sum - FactorizationMachine.userfeature[u][f])
                               FactorizationMachine.itemfeature[i][f] += lRate*error*(sum - FactorizationMachine.itemfeature[i][f])
                               for other in data_hash[u]:
                                   FactorizationMachine.otherMoviesWatchedLatentFeatures[other][f] += lRate*error*(FactorizationMachine.otherMoviesWatchedValues[u][other]*sum - FactorizationMachine.otherMoviesWatchedLatentFeatures[other][f]*math.pow(FactorizationMachine.otherMoviesWatchedValues[u][other],2))
                            AverageRecommender.global_average +=  lRate*error
                            AverageRecommender.user_bias[u] +=  lRate*error
                            AverageRecommender.item_bias[i] += lRate*error
                print(iteration)
    
    def predict(data_hash, user, item):
        result = AverageRecommender.global_average + AverageRecommender.user_bias.get(user,0) + AverageRecommender.item_bias.get(item,0)
        if FactorizationMachine.userfeature.get(user,None) is None or FactorizationMachine.itemfeature.get(item,None) is None: return result
        sum = 0
        for f in range(0,len(FactorizationMachine.userfeature[user])):
            sum2 = FactorizationMachine.userfeature[user][f] + FactorizationMachine.itemfeature[item][f]
            sum3 = math.pow(FactorizationMachine.userfeature[user][f],2) + math.pow(FactorizationMachine.itemfeature[item][f],2)
            for other in data_hash[user]:
                if f==0:
                    result += FactorizationMachine.otherMoviesWatchedFeatures[other]*FactorizationMachine.otherMoviesWatchedValues[user][other]
                sum2 += FactorizationMachine.otherMoviesWatchedLatentFeatures[other][f]*FactorizationMachine.otherMoviesWatchedValues[user][other]
                sum3 += math.pow(FactorizationMachine.otherMoviesWatchedLatentFeatures[other][f],2)*math.pow(FactorizationMachine.otherMoviesWatchedValues[user][other],2)
            sum+=math.pow(sum2,2)-sum3
        return result+0.5*sum