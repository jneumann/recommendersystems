﻿from DataReader import DataReader
from AverageRecommender import AverageRecommender
from KNNRecommender import KNNRecommender
from Evaluator import Evaluator
from MatrixFactorization import MatrixFactorization
from FactorizationMachine import FactorizationMachine

data_hash = DataReader.readCSV("ratings_train.csv")
test_hash = DataReader.readCSV("ratings_test.csv")

AverageRecommender.learn(data_hash)

print("-----")
FactorizationMachine.learn(data_hash,20)
print(Evaluator.root_mean_squared(data_hash, test_hash, FactorizationMachine.predict))
