﻿import math
from AverageRecommender import AverageRecommender


class MatrixFactorization(object):
    
    
    """description of class"""
    userfeature = {}
    itemfeature = {}


    def learn(data_hash , k, lRate=0.01, regRate = 0.01, learn_bias = False):
        for u in data_hash:
            MatrixFactorization.userfeature[u] = []
            for i in data_hash[u]:
                MatrixFactorization.itemfeature[i] = []
                for f in range(0,k):
                    MatrixFactorization.itemfeature[i].append(0.1)
            for f in range(0,k):
                MatrixFactorization.userfeature[u].append(0.1)
        iterations = 30
        for iteration in range(0,iterations):
                errorsum = 0
                errorcount = 0
                for u in data_hash:
                    for i in data_hash[u]:
                            error = (data_hash[u][i] - MatrixFactorization.predict(data_hash, u, i))
                            for f in range(0,len(MatrixFactorization.userfeature[u])):
                                uf = MatrixFactorization.userfeature[u][f]
                                MatrixFactorization.userfeature[u][f] = MatrixFactorization.userfeature[u][f] + lRate * (error*MatrixFactorization.itemfeature[i][f]-regRate*MatrixFactorization.userfeature[u][f])
                                MatrixFactorization.itemfeature[i][f] = MatrixFactorization.itemfeature[i][f] + lRate*(error*uf-regRate*MatrixFactorization.itemfeature[i][f])
                            if(learn_bias):
                                AverageRecommender.global_average = AverageRecommender.global_average + lRate*error
                                AverageRecommender.user_bias[u] = AverageRecommender.user_bias[u] + lRate*error
                                AverageRecommender.item_bias[i] = AverageRecommender.item_bias[i] + lRate*error
                #print(iteration)
    
    def predict(data_hash, user, item):
        result = AverageRecommender.global_average + AverageRecommender.user_bias.get(user,0) + AverageRecommender.item_bias.get(item,0)
        if MatrixFactorization.userfeature.get(user,None) is None or MatrixFactorization.itemfeature.get(item,None) is None: return result
        for f in range(0,len(MatrixFactorization.userfeature[user])):
            result = result + MatrixFactorization.userfeature[user][f]*MatrixFactorization.itemfeature[item][f]
        return result