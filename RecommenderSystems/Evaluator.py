﻿import math

class Evaluator(object):

    def root_mean_squared(data_hash, test_hash, prediction_function):
        result = 0
        c=0 
        for u in test_hash:
            for i in test_hash[u]:
                c=c+1
                result = result + math.pow(test_hash[u][i]-prediction_function(data_hash, u, i),2)
        return math.sqrt(result/c)

