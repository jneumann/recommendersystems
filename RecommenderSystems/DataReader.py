﻿import csv

class DataReader(object):

    def readCSV(name):
        user_arr = {}
        with open(name) as csvfile:
             reader = csv.DictReader(csvfile)
             for row in reader:
                 user_arr[row['UserID']] = user_arr.get(row['UserID'], {})
                 user_arr[row['UserID']][row['MovieID']] = float(row['Rating'])
        return user_arr

    def readCSV2(name):
        user_arr = {}
        with open(name) as csvfile:
             reader = csv.DictReader(csvfile)
             for row in reader:
                 user_arr[row['MovieID']] = user_arr.get(row['MovieID'], {})
                 user_arr[row['MovieID']][row['UserID']] = float(row['Rating'])
        return user_arr